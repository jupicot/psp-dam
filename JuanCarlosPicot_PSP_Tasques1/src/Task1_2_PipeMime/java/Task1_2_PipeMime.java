package Task1_2_PipeMime.java;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Task1_2_PipeMime {
	
	public static void main(String[] args) throws IOException {
		
		ProcessBuilder builderone = new ProcessBuilder("grep","dhcp4");
		ProcessBuilder buildertwo = new ProcessBuilder("tail");
		ProcessBuilder[] allbuilders = {builderone,buildertwo};
		File path = new File("/var/log/syslog");
		try {
			builderone.redirectInput(path);
			buildertwo.redirectOutput(ProcessBuilder.Redirect.INHERIT);
			
			List<Process> listprocess = ProcessBuilder.startPipeline(Arrays.asList(allbuilders));
			Process lastprocess = listprocess.get(listprocess.size()-1);
			lastprocess.waitFor();
		} catch (Exception e) {
			System.err.println(e);
		}
		
	}

}
