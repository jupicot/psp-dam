package Task1_3_Contador.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Task1_3_Contador {
	
	public static void main(String[] args) throws IOException {
		
		ProcessBuilder builder = new ProcessBuilder("wc","-w","-m");
		builder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		try {
			InputStreamReader isr = new InputStreamReader(System.in,"UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String cadena = null;
			System.out.println("Introduce una frase: ");

			while((cadena = br.readLine())!=null && cadena.length()!=0) {
				Process p = builder.start();
				System.out.println("Paraules  Lletres");
				System.out.println("=================");
				OutputStream os = p.getOutputStream();
				OutputStreamWriter osr = new OutputStreamWriter(os,"UTF-8");
				osr.write(cadena);
				osr.flush();
				osr.close();	
				os.close();
				p.waitFor();
				System.out.println("Introduce una frase: ");
			}

		} catch (Exception e) {
			System.err.println(e);
		}
	}

}
