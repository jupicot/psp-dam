package Task1_4_HashMd5redirect.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Task1_4_HashMd5redirect {
	
	public static void main(String[] args) throws InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("md5sum");
		pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		
		try {
			InputStreamReader isr = new InputStreamReader(System.in,"UTF-8");
			BufferedReader br = new BufferedReader (isr);
			String command = "md5sum";
			String cadena = null;
			
			System.out.println("md5txt>");
			while((cadena = br.readLine())!=null && cadena.length()!=0) {
				Process p = pb.start();
				OutputStream os = p.getOutputStream();
				OutputStreamWriter osr = new OutputStreamWriter(os,"UTF-8");
				osr.write(cadena);
				osr.flush();
				osr.close();
				p.waitFor();
				System.out.println("md5txt>");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
