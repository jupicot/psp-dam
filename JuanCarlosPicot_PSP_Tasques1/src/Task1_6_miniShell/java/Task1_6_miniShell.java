package Task1_6_miniShell.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class Task1_6_miniShell {
	public static void main(String[] args) throws IOException {
		String cadena = "";
		do {
			System.out.println("Introduce un comando valido: ");
			InputStreamReader isr = new InputStreamReader(System.in,"UTF-8");
			BufferedReader br = new BufferedReader (isr);
			cadena = br.readLine();
			
			if(cadena.contains("ls") || cadena.contains("grep") || cadena.contains("cat") || cadena.contains("nano")) {
				String[] text = cadena.split(" ");
			ProcessBuilder pb = new ProcessBuilder(text);
			pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
			Process p = pb.start();
			}else {
				System.out.println("exit");
			}
		} while (cadena.contains("ls") || cadena.contains("grep") || cadena.contains("cat") || cadena.contains("nano"));

	}
}
