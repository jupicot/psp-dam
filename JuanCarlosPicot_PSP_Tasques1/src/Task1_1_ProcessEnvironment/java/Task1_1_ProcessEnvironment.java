package Task1_1_ProcessEnvironment.java;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class Task1_1_ProcessEnvironment {
	
	public static void main(String[] args) {
		ArrayList<String> name = new ArrayList<String>();
		ArrayList<String> value = new ArrayList<String>();
		ProcessBuilder pb = new ProcessBuilder();
		Map<String,String> env = pb.environment();
		
		for (String envName : env.keySet()) {
			name.add(envName);
		}
		
		for (String envValue : env.values()) {
			value.add(envValue);
		}
		
		for (int i = 0; i < name.size(); i++) {
			System.out.println("Variable: " + name.get(i) + " | " + "Valor: " + value.get(i));
		}
	}
}
