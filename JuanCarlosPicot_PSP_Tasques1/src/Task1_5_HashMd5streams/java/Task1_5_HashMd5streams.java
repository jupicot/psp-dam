package Task1_5_HashMd5streams.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Task1_5_HashMd5streams {
	public static void main(String[] args) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("md5sum");
		
		try {
			Process p = pb.start();
			InputStreamReader isr = new InputStreamReader(p.getInputStream());
			BufferedReader br = new BufferedReader (isr);
			String command = "md5sum";
			String cadena = null;
			
			System.out.println("md5txt>");
			while((cadena = br.readLine())!=null && cadena.length()!=0) {
				OutputStream os = p.getOutputStream();
				OutputStreamWriter osr = new OutputStreamWriter(os,"UTF-8");
				osr.write(cadena);
				osr.flush();
				osr.close();				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
