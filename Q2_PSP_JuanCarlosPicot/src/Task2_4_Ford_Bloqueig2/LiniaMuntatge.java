package Task2_4_Ford_Bloqueig2;
import java.io.IOException;
import java.util.Arrays;

public class LiniaMuntatge implements Runnable{
	//ATRIBUTS
	private String nom;
	private Component [] component = new Component[3]; //vector de Components

	int numCompReservedToAssembly;	//Num components reservats pel fil
	int numCompFree;		//Num components lliures

	//	CONSTRUCTOR
	public LiniaMuntatge(String nomLinia, Component comp1, Component comp2, Component comp3) {
		nom = nomLinia;
		System.out.println("["+Thread.currentThread().getName()+"] Iniciant de la linia de muntatge "+ nom);
		this.component[0] = comp1;
		this.component[1] = comp2;
		this.component[2] = comp3;

		numCompReservedToAssembly = 0;
		numCompFree = 3;
	}

	//METODES
	public void recullIensambla2Components() {
		for (Component comp : component) {
			//Si component reservat
			if(comp.getReserva()==Thread.currentThread().getName()) {
				comp.setDisponible(false);			//Component deixa estar disponible
				System.out.println("["+Thread.currentThread().getName()+"] Recull component "+ comp.getNomComponent()+" ...");
			}//if
		}//for
		//Recorre components identificant els que s'han d'ensamblar
		int numComps=0;
		for (Component comp : component) {
			if(!comp.isDisponible()) {		//component reservat
				if(numComps==0) {	//mostra 1r component reservat
					System.out.print("["+Thread.currentThread().getName()+"] Ensamblant components "+ comp.getNomComponent());
					numComps++;
				}else {			//mostra 2n component reservat
					System.out.println(" amb "+ comp.getNomComponent() +" ...");
				}//else
			}//if
		}//for
	}//recullIensamblaComponents

	//--------------------------------------------------------------------------------
	public void recullIensamblaUltimComponent() {
		for (Component comp : component) {
			//cerca 3r component
			if(comp.getReserva()==Thread.currentThread().getName() && comp.isDisponible()) {
				comp.setDisponible(false);			//Component deixa estar disponible
				System.out.println("["+Thread.currentThread().getName()+"] Recull component "+ comp.getNomComponent()+" ...");
				System.out.println("["+Thread.currentThread().getName()+"] Ensamblant ultim component "+ comp.getNomComponent());
			}
		}//for
	}//recullIensamblaUltimComponent

	//--------------------------------------------------------------------------------
	public int numComponentsDisponibles(){
		int numComponentsLliures = 0;
		for (Component comp : component) {
			if (comp.isDisponible()) {
				numComponentsLliures++;
			}
		}
		return numComponentsLliures;	
	}//numComponentsDisponibles

	//-------------------------------------------------------------------------------
	public Component CercaIreservaNouComponent() {
		for (Component comp : component) {
			//			//sincronitzacio per comprobar disponibilitat del component i reservar-lo en cas que estiga lliure 
			synchronized(comp) {
				if (comp.isDisponible() && comp.getReserva()=="") { //hi ha components lliures
					System.out.println("["+Thread.currentThread().getName()+"] Reservant component "+ comp.getNomComponent()+"...");
					comp.setReserva(Thread.currentThread().getName());		//anota component reservat pel fil
					numCompReservedToAssembly++;		//incrementa components reservats
					numCompFree--;			//decrementa components lliures
					return comp;		//retorna component reservat
				}//if
			}//sync
		}//for
		return null;
	}
	//-------------------------------------------------------------------------------

	public void run() {
		Component componentLliure1, componentLliure2;
		do {
			System.out.println("["+Thread.currentThread().getName()+"] Esperant NOU component");
			//Si hi ha un component disponible, tracta de reservar un segon
			if ((componentLliure1 = CercaIreservaNouComponent())!=null) {
				synchronized(componentLliure1) {
					if ((componentLliure2 = CercaIreservaNouComponent())!=null) {
						synchronized(componentLliure2) {
							//fil té un mínim de 2 Components		

							//recull i ensambla les 2 primers Components
							recullIensambla2Components();

							//reserva nou component
							componentLliure1 = CercaIreservaNouComponent();
							//si queda component a ensamblar
							if(componentLliure1!= null) {
								//recull i ensambla les 2 primers Components
								recullIensamblaUltimComponent();
								System.out.println("["+Thread.currentThread().getName()+"] Cotxe MUNTAT!");
							}

						}//sync
					}
				}//sync
			}//if


			if(numCompReservedToAssembly < 2) {	//Si no s'ha pogut reservar 2 components
				//S'allibera el component reservat, si hi havia algun
				for (Component comp : component) {
					// alliberar component 
					if(comp.getReserva()==Thread.currentThread().getName()) {
						System.out.println("["+Thread.currentThread().getName()+"] Alliberant component "+ comp.getNomComponent()+"...");
						comp.setReserva("");
						numCompReservedToAssembly--;	//decrementa components reservats
						numCompFree++;				//incrementa components lliures
					}//if
				}//for
			}//if


			//Si no queden suficients components per fer un ensamblatge, ixim del bucle do..while
			if(numComponentsDisponibles()<=1) {
				break;
			}//if
		}while(numCompReservedToAssembly < 2 && numCompFree > 1 );

		System.out.println("["+Thread.currentThread().getName()+"] Final fil");
	}//run
}//class
