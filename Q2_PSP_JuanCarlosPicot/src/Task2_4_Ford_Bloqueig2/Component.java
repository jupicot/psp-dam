package Task2_4_Ford_Bloqueig2;
import java.io.*;

public class Component{
	//attr
	private String nomComponent;
	private boolean disponible;
	private String reserva;

	//CONSTRUCTOR 1
	public Component( String nom) throws IOException {
		try {
			this.setNomComponent(nom);
			System.out.println("[main] Encarregant  1 component "+ getNomComponent()+" a dept VENDES...");
			Thread.sleep(30);		//Temps fins rebre component de proveedors
			System.out.println("[main] Nou component " + getNomComponent() + " disponible");
			disponible = true;
			reserva = "";
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getNomComponent() {
		return nomComponent;
	}

	public void setNomComponent(String nomComponent) {
		this.nomComponent = nomComponent;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		try {
			Thread.sleep(1);
			this.disponible = disponible;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getReserva() {
		return reserva;
	}

	public void setReserva(String reservat) {
		this.reserva = reservat;
	}
	
/*	public boolean isReservat() {
		if(reservat == "") return false;
		else return true;
	}
*/	

}
