package Task2_4_Ford_Bloqueig2;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("Programa muntatge COTXES de FORD INTERBLOQUEIG \n================================");
		Component carrosseria = new Component("CARROSSERIA");
		Component motor = new Component("MOTOR");
		Component rodes = new Component("RODES");

		String nomL1 = "liniaA";
		String nomL2 = "liniaB";
		String nomL3 = "liniaC";
		
		LiniaMuntatge obj1 = new LiniaMuntatge(nomL1, carrosseria, motor, rodes);
		LiniaMuntatge obj2 = new LiniaMuntatge(nomL2, motor, rodes, carrosseria);
		LiniaMuntatge obj3 = new LiniaMuntatge(nomL3, rodes, carrosseria, motor);

		Thread fil1 = new Thread(obj1, nomL1);
		Thread fil2 = new Thread(obj2, nomL2);
		Thread fil3 = new Thread(obj3, nomL3);

		fil1.start();
		fil2.start();
		fil3.start();
		
		fil1.join();
		fil2.join();
		fil3.join();
				
		System.out.println("[main] Finalitzacio del programa ppal");
	}

}
