package Task2_5_Supermercat_1caixa_waitNotify;

import java.text.DecimalFormat;

public class Client implements Runnable{
	
	float compra;
	String client;
	Caixa caixa;
	DecimalFormat formato = new DecimalFormat("#.##");
	
	public Client() {
		super();
	}

	public Client(String client, Caixa caixa) {
		super();
		this.client = client;
		this.caixa = caixa;
	}

	public float getCompra() {
		return compra;
	}

	public void setCompra(float compra) {
		this.compra = compra;
	}
	
	

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}
	
	public void recollida(String nomfil) {
		System.out.println("[" + nomfil + "] " + "El " + nomfil + " ha fet la recollida de productes");
	}
	
	public void vaCaixa(String nomfil) {
		System.out.println("[" + nomfil + "] " + nomfil + " va a la " + caixa.getCaixa());
	}

	@Override
	public void run() {
		
		try {
			compra = (float) (5 + Math.random() * (10 - 5));
			recollida(Thread.currentThread().getName());
			vaCaixa(Thread.currentThread().getName());
			Thread.currentThread().sleep(100);
			caixa.encuantseCola(Thread.currentThread().getName());
			caixa.metodeWait(Thread.currentThread().getName());
			caixa.cobramentCompra(formato.format(compra), client);
			caixa.ticketCompra(Thread.currentThread().getName());
			caixa.recollintCompra(Thread.currentThread().getName());
			caixa.canviaestatdisponible(Thread.currentThread().getName());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
