package Task3_1_consultaEquip;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		boolean salir = false;
		int opcion;
		
		while(!salir) {
			System.out.println("");
			System.out.println("1. Buscar por IP");
			System.out.println("2. Buscar por nombre");
			System.out.println("3. Buscar localhost");
			System.out.println("4. Buscar localhost especial");
			System.out.println("5. Salir");
			System.out.println("");
			System.out.println("Escribe el número de la opción que quieres consultar");
			opcion = scanner.nextInt();
			Scanner s = new Scanner(System.in);

			switch(opcion) {
				case 1:
					System.out.println("Introduce la IP que quieres buscar: ");
					String webip = s.nextLine();
					System.out.println("==== Printing Info for: 'By-Name(" + webip + ")'====");
					getInfo(webip);
					break;
				case 2:
					System.out.println("Introduce el host name que quieres buscar: ");
					String webname = s.nextLine();
					System.out.println("==== Printing Info for: 'By-Name(" + webname + ")'====");
					getInfo(webname);
					break;
				case 3:
					String localip = "127.0.0.1";
					System.out.println("==== Printing Info for: 'Localhost'====");
					getInfo(localip);
					break;
				case 4:
					String specialip = "127.0.1.1";
					System.out.println("==== Printing Info for: 'Special Localhost'====");
					getInfo(specialip);
					break;
				case 5:
					salir = true;
					break;
				default:
					System.out.println("Introduce un número entre el 1 y el 5.");
			}
		}
		}
	/*	if(mode.equals("ip")) {
			System.out.println("Introduce la IP que quieres buscar: ");
			String web = scanner.nextLine();
			System.out.println("==== Printing Info for: 'By-Name(" + web + ")'====");
			getInfo(web);
		}else if(mode.equals("name")) {
			System.out.println("Introduce el host name que quieres buscar: ");
			String web = scanner.nextLine();
			System.out.println("==== Printing Info for: 'By-Name(" + web + ")'====");
			getInfo(web);
		}else if(mode.equals("localhost")) {
			String localip = "127.0.0.1";
			System.out.println("==== Printing Info for: 'Localhost'====");
			getInfo(localip);
		}else if(mode.equals("special localhost")) {
			String specialip = "127.0.1.1";
			System.out.println("==== Printing Info for: 'Special Localhost'====");
			getInfo(specialip);
		}
	}*/
	
	 public static void getInfo(String newip) throws IOException {
			try {
				InetAddress ip = InetAddress.getByName(newip);
				System.out.println("Host Name: " + ip.getHostName());
				System.out.println("Canonical Host Name: " + ip.getCanonicalHostName());
				System.out.println("Host Address: " + ip.getHostAddress());
				System.out.print("Address: ");
				byte[] bytes = ip.getAddress();
				for (byte b : bytes) {
					System.out.print(b & 0xFF);
					System.out.print(".");
				}
				System.out.println("");
				System.out.println("Calculated Host Address: " + ip.getHostAddress());
				System.out.println("Is Any Local?" + ip.isAnyLocalAddress());
				System.out.println(" Is Link Local?" + ip.isLinkLocalAddress());
				System.out.println(" Is Loopback?" + ip.isLoopbackAddress());
				System.out.println(" Is Multicast?" + ip.isMulticastAddress());
				System.out.println(" Is Site Local?" + ip.isSiteLocalAddress());
				System.out.println(" Is Reachable in 2 seconds?" + ip.isReachable(2000));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
	 }
}
