package Task2_3_CitaPrevia_2Enfermeres;

import java.util.ArrayList;
import java.util.List;

public class Infermera {
	
	private String nombre;
	public QuadreCites quadre = new QuadreCites();
	public ArrayList<String> horarios = new ArrayList<String>();
	
	public Infermera(String nombre) {
		super();
		this.nombre = nombre;
		horarios = quadre.getHorarios();
	}
	

	public Infermera() {
		super();
	}


	public String getNombre() {
		return nombre;
	}


	public void setInfermera(String nombre) {
		this.nombre = nombre;
	}


	public QuadreCites getQuadre() {
		return quadre;
	}


	public void setQuadre(QuadreCites quadre) {
		this.quadre = quadre;
	}

	
	public synchronized void asignaCita(String nomFil,String pacient, Pacient paciente, Thread currentThread) throws InterruptedException {
		for(int i = 0; i < horarios.size(); i++ ) {
				if(!horarios.isEmpty()) {
					System.out.println("[" + nomFil + "] " + nombre + " assigna la cita previa per a " + pacient + " a les " + horarios.get(0));
					paciente = new Pacient();
					horarios.remove(0);
					currentThread.sleep(1000);
					paciente.pararThread();
				}
		}
	}
}
