package Task2_3_CitaPrevia_2Enfermeres;


public class Main {
	
	public static void main(String[] args) throws InterruptedException {		
		
		Infermera infermera = new Infermera("Infermera");
		Infermera infermera2 = new Infermera("Infermera2");
		Pacient pacient1 = new Pacient("pacient1", infermera);
		Pacient pacient2 = new Pacient("pacient2", infermera2);
		Pacient pacient3 = new Pacient("pacient3", infermera);
		Pacient pacient4 = new Pacient("pacient4", infermera2);
		Pacient pacient5 = new Pacient("pacient5", infermera);
		Pacient pacient6 = new Pacient("pacient6", infermera2);
		Pacient pacient7 = new Pacient("pacient7", infermera);
		Pacient pacient8 = new Pacient("pacient8", infermera2);
		Pacient pacient9 = new Pacient("pacient9", infermera);


		Thread fil1 = new Thread(pacient1, "pacient1");
		Thread fil2 = new Thread(pacient2, "pacient2");
		Thread fil3 = new Thread(pacient3, "pacient3");
		Thread fil4 = new Thread(pacient4, "pacient4");
		Thread fil5 = new Thread(pacient5, "pacient5");
		Thread fil6 = new Thread(pacient6, "pacient6");
		Thread fil7 = new Thread(pacient7, "pacient7");
		Thread fil8 = new Thread(pacient8, "pacient8");
		Thread fil9 = new Thread(pacient9, "pacient9");
		
		System.out.println("[main] Inicialitza quadre de Cita Previa");
		System.out.println("[main] Infermera agafa el quadre de Cita Previa");

		
		fil1.start();
		fil2.start();
		fil3.start();
		fil4.start();
		fil5.start();
		fil6.start();
		fil7.start();
		fil8.start();
		fil9.start();
		
		
		fil1.join();
		fil2.join();
		fil3.join();
		fil4.join();
		fil5.join();
		fil6.join();
		fil7.join();
		fil8.join();
		fil9.join();
 
	}

}
