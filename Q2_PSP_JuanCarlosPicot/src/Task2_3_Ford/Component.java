package Task2_3_Ford;

public class Component {
	
	private String carrosseria;
	private String motor;
	private String rodes;
	
	
	
	public Component(String carrosseria, String motor, String rodes) {
		super();
		this.carrosseria = carrosseria;
		this.motor = motor;
		this.rodes = rodes;
	}


	public String getCarrosseria() {
		return carrosseria;
	}


	public void setCarrosseria(String carrosseria) {
		this.carrosseria = carrosseria;
	}


	public String getMotor() {
		return motor;
	}


	public void setMotor(String motor) {
		this.motor = motor;
	}


	public String getRodes() {
		return rodes;
	}


	public void setRodes(String rodes) {
		this.rodes = rodes;
	}

}
