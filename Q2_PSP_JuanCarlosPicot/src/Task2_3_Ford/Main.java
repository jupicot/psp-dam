package Task2_3_Ford;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		
		Component component = new Component("carrosseria","motor","rodes");
		
		LiniaMuntatge linia = new LiniaMuntatge(component);

		Thread fil1 = new Thread(linia, "liniaA");
		Thread fil2 = new Thread(linia, "liniaB");
		Thread fil3 = new Thread(linia, "liniaC");
		
		System.out.println("Programa muntatge COTXES de FORD");
		System.out.println("================================");
		System.out.println("[main] " + "Encarregant 1 component " + component.getCarrosseria() + " a dept VENDES..");
		System.out.println("[main] Component " + component.getCarrosseria() + " disponible");
		System.out.println("[main] " + "Encarregant 1 component " + component.getMotor() + " a dept VENDES..");
		System.out.println("[main] Component " + component.getMotor() + " disponible");
		System.out.println("[main] " + "Encarregant 1 component " + component.getRodes() + " a dept VENDES..");
		System.out.println("[main] Component " + component.getRodes() + " disponible");
		System.out.println("[main] Iniciant de la linia de muntatge " + fil1.getName());
		System.out.println("[main] Iniciant de la linia de muntatge " + fil2.getName());
		System.out.println("[main] Iniciant de la linia de muntatge " + fil3.getName());

		fil1.start();
		try {
			fil1.sleep(30);
		} catch (InterruptedException e) {
			System.out.println("Error en el hilo 1: " + e);
			}
		
		fil2.start();
		try {
			fil2.sleep(30);
		} catch (InterruptedException e) {
			System.out.println("Error en el hilo 2: " + e);
			}
		
		fil3.start();
		try {
			fil3.sleep(30);
		} catch (InterruptedException e) {
			System.out.println("Error en el hilo 3: " + e);
			}
		
		fil1.join();
		fil2.join();
		fil3.join();
		
		System.out.println("[main] Finalització del programa ppal");

	}

}
