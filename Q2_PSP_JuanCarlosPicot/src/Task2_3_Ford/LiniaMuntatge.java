package Task2_3_Ford;

public class LiniaMuntatge implements Runnable {
	
	private int cont = 0;
	private String v = "";
	private Component component;

	public LiniaMuntatge(Component component) {
		super();
		this.component = component;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}
	
	public void cerca(Component component) {
		System.out.println("Cerca component" + component.getCarrosseria());
	}
	
	public synchronized void cerca(Component component, String nomFil) {
		System.out.println("[" + nomFil + "]" + " Cerca component " + component.getCarrosseria());
		try {
			Thread.currentThread().sleep(30);

		} catch (InterruptedException e) {
			System.out.println("Error en la cerca del component: " + e);
		}
	}
	
	public synchronized void recull(Component component, String nomFil) {
		try {
			cont++;
			if(cont==1) {
				System.out.println("[" + nomFil + "]" + " Recull component " + component.getCarrosseria());
				cont++;
				v = nomFil;
				Thread.currentThread().sleep(60);
			}else {
				System.out.println("[" + nomFil + "]" + " Esperant NOU component " + component.getCarrosseria());
				System.out.println("[" + nomFil + "]" +" Linia BLOQUEJADA");
				Thread.currentThread().sleep(60);
			}
		} catch (InterruptedException e) {
			System.out.println("Error en la recollida del component: " + e);
		}
	}
	
	public synchronized void ensamblar(Component component, String nomFil, String v) {
		try {
		if(v == nomFil) {
			System.out.println("[" + nomFil + "]" + " Ensambla component " + component.getCarrosseria());
			System.out.println("[" + nomFil + "]" + " Cerca component " + component.getMotor());
			System.out.println("[" + nomFil + "]" + " Recull component " + component.getMotor());
			System.out.println("[" + nomFil + "]" + " Ensambla component " + component.getMotor());
			System.out.println("[" + nomFil + "]" + " Cerca component " + component.getRodes());
			System.out.println("[" + nomFil + "]" + " Recull component " + component.getRodes());
			System.out.println("[" + nomFil + "]" + " Ensambla component " + component.getRodes());
			System.out.println("COTXE MUNTAT!");
		}
		Thread.currentThread().sleep(100);
		} catch (InterruptedException e) {
			System.out.println("COTXE NO MUNTAT!");
		}
			
	}

	@Override
	public void run() {
		
		try {
			cerca(component, Thread.currentThread().getName());
			recull(component, Thread.currentThread().getName());
			//component.encarregant(component, Thread.currentThread().getName());
			Thread.currentThread().sleep(100);

			if(v.length() > 0) {
			ensamblar(component, Thread.currentThread().getName(), v);
			}
			
		} catch (Exception e) {
			

		}
		
	}
	
	
	
}
