package Task3_3_Login;

import java.net.InetAddress;

public class Main {

	public static void main(String[] args) {
		try {
			int portServidor = 15017;
			String ipServidor = InetAddress.getByName("localhost").getHostAddress().toString();
			int portClient = 15018;
			int portClient2 = 15019;
			Thread serverUDP = new Thread(new ServidorUDP(portServidor),"ServerUDP");
			Thread clientUDP = new Thread(new ClientUDP(portClient,portServidor,ipServidor),"ClientUDP");
			Thread clientUDP2 = new Thread(new ClientUDP(portClient2,portServidor,ipServidor),"ClientUDP2");
			//Thread serverUDP2 = new Thread(new ServidorUDP(portServidor,portClient2),"ServerUDP");
			//Thread.sleep(1000);
			System.out.println("[" + serverUDP.getName() +"] SERVIDOR escoltant al port " + portServidor);
			serverUDP.start();
			System.out.println("["+ clientUDP.getName() + "] CLIENT.");
			System.out.println("["+ clientUDP.getName() + "] IP del servidor: " + ipServidor);
			System.out.println("["+ clientUDP.getName() + "] Port del servidor: " + portServidor);
			clientUDP.start();
			synchronized(clientUDP) {
				clientUDP.wait();
			}
			System.out.println("["+ clientUDP2.getName() + "] CLIENT.");
			System.out.println("["+ clientUDP2.getName() + "] IP del servidor: " + ipServidor);
			System.out.println("["+ clientUDP2.getName() + "] Port del servidor: " + portServidor);
			clientUDP2.start();
			clientUDP2.join();
			//serverUDP.join();
			//serverUDP2.start();
			//serverUDP2.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
