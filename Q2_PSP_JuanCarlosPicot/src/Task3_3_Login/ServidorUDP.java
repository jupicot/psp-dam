package Task3_3_Login;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;

public class ServidorUDP implements Runnable{

	private int portServidor;
	private DatagramSocket lsocket;
	private DatagramPacket datagrama;
	private ArrayList<String> usuarios = new ArrayList<String>();
	private String contra = "l123";
	private String usuari;
	private String contrassenya;
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";
	public ServidorUDP(int portServidor) {
		this.portServidor = portServidor;
		this.usuarios.add("laura");
		this.usuarios.add("paco");
	}
	
	public void recibir() throws IOException {
		byte[] bytesArebre = new byte[1024];

		datagrama = new DatagramPacket(bytesArebre,
		bytesArebre.length);
		lsocket = new DatagramSocket(portServidor);
		lsocket.receive(datagrama);
	
		if(!lsocket.isClosed())
		lsocket.close();
		usuari = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");
	}
	public void usuarioExiste() throws IOException {
		lsocket = new DatagramSocket();
		String missatge = "Usuari " + usuari + " existeix";
		System.out.println("[" + Thread.currentThread().getName() + "] " + missatge);
		datagrama = new DatagramPacket(missatge.getBytes("UTF-8"),
				missatge.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
	}
	
	public void usuarioNOExiste() throws IOException {
		lsocket = new DatagramSocket();
		String missatge = "Usuari " + usuari + " NO existeix";
		System.out.println("[" + Thread.currentThread().getName() + "] " + missatge);
		datagrama = new DatagramPacket(missatge.getBytes("UTF-8"),
				missatge.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
		}
	
	public void loginFallido() throws IOException {
		lsocket = new DatagramSocket();
		String missatge = "Ho sentim, no se admiteixen mes intents d'inici de sesio.";
		datagrama = new DatagramPacket(missatge.getBytes("UTF-8"),
				missatge.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
	}
	
	public void recibirContra() throws IOException {
		byte[] bytesArebre = new byte[1024];

		datagrama = new DatagramPacket(bytesArebre,
		bytesArebre.length);
		lsocket = new DatagramSocket(portServidor);
		lsocket.receive(datagrama);
	
		if(!lsocket.isClosed())
		lsocket.close();
		contrassenya = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");
		if(contra.equals(contrassenya)) {
			validarUsuario();
		}else {
			noValidarUsuario();
		}
	}
	
	public void validarUsuario() throws IOException {
		//Muestro el puerto con getSocketAddress para diferenciar los cientes ya que ambos están en la ip local
		System.out.println("[" + Thread.currentThread().getName() + "] Usuari " + usuari +" validat correctament des de la ip " + datagrama.getSocketAddress());
		lsocket = new DatagramSocket();
		String missatge = "Usuari " + usuari + " connectat correctament";
		datagrama = new DatagramPacket(missatge.getBytes("UTF-8"),
				missatge.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
	}
	
	public void noValidarUsuario() throws IOException {
		System.out.println("[" + Thread.currentThread().getName() +  "] Usuari " + usuari +" no validat, contrassenya incorrecta");
		lsocket = new DatagramSocket();
		String missatge = "Usuari " + usuari + ", login incorrecte";
		datagrama = new DatagramPacket(missatge.getBytes("UTF-8"),
				missatge.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
	}

	@Override
	public void run() {
		while (true) {
			try {		
				recibir();
				if(usuarios.contains(usuari)) {
					usuarioExiste();
					recibirContra();
				}else {
					usuarioNOExiste();
					recibir();
					if(usuarios.contains(usuari)) {
						usuarioExiste();
						recibirContra();
						
					}else {
						loginFallido();
					}
				}
				
				} catch (Exception e) {
				e.printStackTrace();
				}			
		}

	}
}
