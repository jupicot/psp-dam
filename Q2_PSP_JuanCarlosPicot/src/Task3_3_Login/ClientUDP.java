package Task3_3_Login;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientUDP implements Runnable,Serializable{
	
	private int portClient;
	private int portServidor;
	private String ipServidor;
	private String usuari;
	private String ipClient;
	private int randomNum;
	private DatagramSocket lsocket;
	private DatagramPacket datagrama;
	public ClientUDP(int portClient, int portServidor, String ipServidor) throws UnknownHostException {
		this.portClient = portClient;
		this.portServidor = portServidor;
		this.ipServidor = ipServidor;
		this.ipClient = InetAddress.getByName("localhost").getHostAddress().toString();

	}
	
	public void enviarContra(String nombre,byte[] bytesArebre) throws IOException, InterruptedException {
		System.out.print("[" + Thread.currentThread().getName() + "] Contrassenya per usuari " + nombre + ": ");
		Scanner s5 = new Scanner(System.in);
		String cadenaAenviar5 = s5.nextLine();
		
		byte[] missatge5 = cadenaAenviar5.getBytes("UTF-8");
		
		lsocket = new DatagramSocket(portClient);
		datagrama = new DatagramPacket(missatge5,
		missatge5.length, InetAddress.getByName("127.0.0.1"), portServidor);
		lsocket.send(datagrama);
		if(!lsocket.isClosed())
		lsocket.close();
		
		datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);
		lsocket = new DatagramSocket(portClient);
		lsocket.receive(datagrama);

		String missatgee6 = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");
		System.out.println("["+ Thread.currentThread().getName() + "] " + missatgee6);
		if(!lsocket.isClosed())
			lsocket.close();
	}
	

	@Override
	public void run() {
		try {
			Thread.currentThread().sleep(100);
			System.out.print("[" + Thread.currentThread().getName() + "] Usuari a enviar al servidor: ");
			Scanner s = new Scanner(System.in);
			String cadenaAenviar = s.nextLine();
			if(cadenaAenviar.equals("salir")) {
				synchronized(Thread.currentThread()) {
					Thread.currentThread().notifyAll();
				}
			}else {
			
			byte[] missatge = cadenaAenviar.getBytes("UTF-8");
			lsocket = new DatagramSocket(portClient);

			datagrama = new DatagramPacket(missatge,
			missatge.length, InetAddress.getByName("127.0.0.1"), portServidor);
			lsocket.send(datagrama);
			if(!lsocket.isClosed())
			lsocket.close();
			
			byte[] bytesArebre = new byte[1024];

			datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);
			lsocket = new DatagramSocket(portClient);
			lsocket.receive(datagrama);

			String missatgee = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");
			System.out.println("["+ Thread.currentThread().getName() + "] " + missatgee);
			if(!lsocket.isClosed())
				lsocket.close();
			
			if(missatgee.contains("NO existeix")) {
				System.out.print("[" + Thread.currentThread().getName() + "] Proporciona un usuari vàlid: ");
				Scanner s3 = new Scanner(System.in);
				String cadenaAenviar3 = s3.nextLine();
				byte[] missatge3 = cadenaAenviar3.getBytes("UTF-8");
				lsocket = new DatagramSocket(portClient);

				datagrama = new DatagramPacket(missatge3,
				missatge3.length, InetAddress.getByName("127.0.0.1"), portServidor);
				lsocket.send(datagrama);
				if(!lsocket.isClosed())
				lsocket.close();
				
				datagrama = new DatagramPacket(bytesArebre, bytesArebre.length);
				lsocket = new DatagramSocket(portClient);
				lsocket.receive(datagrama);

				String missatgee4 = new String(datagrama.getData(), 0, datagrama.getLength(), "UTF-8");
				System.out.println("["+ Thread.currentThread().getName() + "] " + missatgee4);
				if(!lsocket.isClosed())
					lsocket.close();
				if(missatgee4.contains("NO existeix")){
					System.out.println(missatgee4);
				}else {
					enviarContra(cadenaAenviar3,bytesArebre);
				}
			
			}else {
				enviarContra(cadenaAenviar,bytesArebre);
			synchronized(Thread.currentThread()) {
				Thread.currentThread().notifyAll();
			}
			}
			}
	} catch (IOException | InterruptedException e) {
		e.printStackTrace();
	}
	}
}
