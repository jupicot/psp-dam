package Task3_2_DialegUDP_adeu;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientUDP implements Runnable{
	
	private int portClient;
	private int portServidor;
	private int randomNum;

	public ClientUDP(int portClient, int portServidor) {
		this.portClient = portClient;
		this.portServidor = portServidor;
		System.out.println("[Client] PORT DEL SERVIDOR " + portServidor);
	}

	public synchronized void enviar() throws InterruptedException, IOException {
		String cadenaAenviar = "Data " + Thread.currentThread().getName();

		byte[] missatge = cadenaAenviar.getBytes("UTF-8");
		DatagramSocket lsocket = new DatagramSocket(portClient);

		DatagramPacket datagrama = new DatagramPacket(missatge,
		missatge.length, InetAddress.getByName("127.0.0.1"), portServidor);
		randomNum = 0 + (int)(Math.random() * 20000);
		System.out.println(randomNum);
		Thread.currentThread().sleep((long) (randomNum));
		if(randomNum > 10000) {
			System.out.println("["+ Thread.currentThread().getName() + "] Ha intentado enviar un mensaje demasiado tarde ");
			lsocket.close();
			Thread.currentThread().join();
		}else {
			System.out.println("["+ Thread.currentThread().getName() + "] Enviant datagrama al port " + portServidor + "...");
			lsocket.send(datagrama);
			if(!lsocket.isClosed())
			lsocket.close();
			
			recibir();
		}
	}
	
	public synchronized void recibir() throws IOException, InterruptedException {
		byte[] bytesArebre = new byte[1024];

		DatagramPacket datagrama2 = new DatagramPacket(bytesArebre, bytesArebre.length);
		DatagramSocket lsocket2 = new DatagramSocket(portClient);
		lsocket2.receive(datagrama2);

		String missatgee = new String(datagrama2.getData(), 0, datagrama2.getLength(), "UTF-8");
		System.out.println("["+ Thread.currentThread().getName() + "] Text rebut: " + missatgee);
		if(missatgee.equals("Adeu")) {
			if(!lsocket2.isClosed())
				lsocket2.close();
			Thread.currentThread().join();
		}
	}
	@Override
	public void run() {
		try {
			enviar();
	} catch (IOException | InterruptedException e) {
		e.printStackTrace();
	}
	}
}
