package Task3_2_DialegUDP_adeu;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class ServidorUDP implements Runnable{

	private int portServidor;
	private int portClient;
	private int peticiones = 0;
	private DatagramSocket lsocket;
	public ServidorUDP(int portServidor,int portClient) throws SocketException {
		this.portServidor = portServidor;
		this.portClient = portClient;
		System.out.println("[Servidor] PORT DEL CLIENT " + portClient);
	}

	@Override
	public void run() {
		while (true) {
			try {
				byte[] bytesArebre = new byte[1024];
				DatagramPacket datagrama = new DatagramPacket(bytesArebre,
				bytesArebre.length);
				lsocket = new DatagramSocket(portServidor);
				lsocket.setSoTimeout(10000);
				System.out.println("["+Thread.currentThread().getName()+"] Esperant datagrama al port " + portServidor + "...");

				try {
					lsocket.receive(datagrama);
					Thread.currentThread().sleep(100);
					peticiones+=1;
				}catch(SocketTimeoutException e){
					lsocket.close();
					if(peticiones < 2)
					System.out.println("Servidor cerrado por falta de peticiones.");
					
					Thread.currentThread().join();
					return;
				}
				if(!lsocket.isClosed())
				lsocket.close();
				
				String missatge = new String(datagrama.getData(), 0,
				datagrama.getLength(), "UTF-8");
				System.out.println("["+Thread.currentThread().getName()+"] Text rebut: " + missatge);
				lsocket = new DatagramSocket();
				String missatgeAdeu = "Adeu";
				DatagramPacket datagrama2 = new DatagramPacket(missatgeAdeu.getBytes("UTF-8"),
						missatgeAdeu.length(), InetAddress.getByName("127.0.0.1"), datagrama.getPort());

				lsocket.send(datagrama2);
				if(!lsocket.isClosed())
				lsocket.close();
				} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				}			
		}

	}
}
