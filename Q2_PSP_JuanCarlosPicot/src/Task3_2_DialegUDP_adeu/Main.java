package Task3_2_DialegUDP_adeu;

public class Main {

	public static void main(String[] args) {
		try {
			int portServidor = 51122;
			int portClient = 41148;
			int portClient2 = 41142;

			Thread clientUDP = new Thread(new ClientUDP(portClient,portServidor),"ClientUDP");
			Thread clientUDP2 = new Thread(new ClientUDP(portClient2,portServidor),"ClientUDP2");
			Thread serverUDP = new Thread(new ServidorUDP(portServidor,portClient),"ServerUDP");

			serverUDP.start();
			clientUDP.start();
			clientUDP2.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}