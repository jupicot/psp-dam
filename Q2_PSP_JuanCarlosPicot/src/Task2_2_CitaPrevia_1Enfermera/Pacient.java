package Task2_2_CitaPrevia_1Enfermera;

import java.util.ArrayList;
import java.util.List;

public class Pacient implements Runnable {
	
	private Pacient paciente;
	private String pacient;
	private Infermera infermera;
	private volatile boolean disponible = true;

	public Pacient(String pacient, Infermera infermera) {
		super();
		this.pacient = pacient;
		this.infermera = infermera;
	}

	public Pacient() {
		// TODO Auto-generated constructor stub
	}

	public String getPacient() {
		return pacient;
	}

	public void setPacient(String pacient) {
		this.pacient = pacient;
	}

	public Infermera getInfermera() {
		return infermera;
	}

	public void setInfermera(Infermera infermera) {
		this.infermera = infermera;
	}
	
	
	public void pararThread() {
		disponible = false;
	}
	

	public synchronized void entra(String nomFil) throws InterruptedException {
		System.out.println("[" + nomFil + "] Entrant a l'ambulatori");		
	}
	
	public synchronized void cita(String nomFil) throws InterruptedException {
		if(!infermera.horarios.isEmpty()) {
			System.out.println("[" + nomFil + "] Sol·licitant cita prèvia a infermera");
			System.out.println("[" + nomFil + "] Infermera busca un espai al quadre de cita previa per a " + pacient);
			infermera.asignaCita(nomFil, pacient, paciente, Thread.currentThread());
			System.out.println("[" + nomFil + "] Eixint de l'ambulatori " + pacient);
		}
	}

	
	public void run() {
		
		try {
			entra(Thread.currentThread().getName());
			Thread.currentThread().sleep((long) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(disponible) {
		try {
			cita(Thread.currentThread().getName());
			Thread.currentThread().sleep((long) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
			}
		}
	}
}
