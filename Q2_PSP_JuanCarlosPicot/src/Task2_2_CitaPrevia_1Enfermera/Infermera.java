package Task2_2_CitaPrevia_1Enfermera;

import java.util.ArrayList;
import java.util.List;

public class Infermera {
	
	private String infermera;
	public QuadreCites quadre = new QuadreCites();
	public ArrayList<String> horarios = new ArrayList<String>();
	
	public Infermera(String infermera) {
		super();
		this.infermera = infermera;
		horarios = quadre.getHorarios();
	}
	

	public Infermera() {
		super();
	}


	public String getInfermera() {
		return infermera;
	}


	public void setInfermera(String infermera) {
		this.infermera = infermera;
	}


	public QuadreCites getQuadre() {
		return quadre;
	}


	public void setQuadre(QuadreCites quadre) {
		this.quadre = quadre;
	}

	

	public void asignaCita(String nomFil,String pacient, Pacient paciente, Thread currentThread) throws InterruptedException {
		ArrayList<String> pacientes = new ArrayList<String>();
		ArrayList<String> horas = horarios;
		for(int i = 0; i < horarios.size(); i++ ) {
				if(!horarios.isEmpty()) {
					System.out.println("[" + nomFil + "] Infermera assigna la cita previa per a " + pacient + " a les " + horarios.get(0));
					pacientes.add(pacient);
					paciente = new Pacient();
					horarios.remove(0);
					currentThread.sleep(1000);
					paciente.pararThread();
				}
		}
		for(int e = 0; e < horas.size(); e++) {
			System.out.println(horas.get(e) + "->" + pacientes.get(e));
		}
	}

}
