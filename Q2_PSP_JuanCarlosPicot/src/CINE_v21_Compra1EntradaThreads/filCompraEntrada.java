package CINE_v21_Compra1EntradaThreads;

import java.math.BigDecimal;
import java.util.ArrayList;

public class filCompraEntrada implements Runnable {

	int pelicula;
	ArrayList<Pelicula> pelicules;

	
	public filCompraEntrada(int pelicula, ArrayList<Pelicula> pelicules) {
		super();
		this.pelicula = pelicula;
		this.pelicules = pelicules;
	}


	public int getPelicula() {
		return pelicula;
	}



	public void setPelicula(int pelicula) {
		this.pelicula = pelicula;
	}

	// ---------------------------------
		//COMPRA INTERACTIVA D'UNA UNICA ENTRADA
		public synchronized void compraEntradaPelicula(){
			int sessions;
			int fila;
			int numSeient;

				pelicules.get(pelicula).toString();
				System.out.println(pelicules.get(pelicula).toString());
				if(pelicules.get(pelicula).llistarSessionsPeli() == 0) {
					System.out.println("\n\t No hi ha cap SESSIÓ per a aquesta pel·licula");
				}else {
					System.out.println(pelicules.get(pelicula).llistarSessionsPeli());
					sessions = Validacio.validaSencer("[" + Thread.currentThread().getName() + "]" + "\t Tria la SESSIÓ  [0 -> Cancel·lar acció]:", pelicules.get(pelicula).llistarSessionsPeli());
					Sessio sessio = pelicules.get(pelicula).retornaSessioPeli(sessions);
					sessio.mapaSessio();
					fila = Validacio.validaSencer("[" + Thread.currentThread().getName() + "]" + "\t Tria FILA: [1-5]  [0 -> Cancel·lar acció]:", sessio.getSala().getFiles());
					numSeient = Validacio.validaSencer("[" + Thread.currentThread().getName() + "]" + "\t Tria SEIENT en la fila: [1-5]  [0 -> Cancel·lar acció]:", sessio.getSala().getTamanyFila());
					Seient seient = sessio.getSeients()[fila-1][numSeient-1];
					seient.reservaSeient();
					System.out.println("Seient reservat");

					if(pagamentEntrada(sessio.getPreu())) {
						seient.ocupaSeient();
						sessio.mapaSessio();
						System.out.println("Pagament confirmat");
					}else {
						seient.alliberaSeient();
						System.out.println("Pagament no processat");
					}
					
				}
		}//compraEntradaPelicula

		


		//*********************************************************
		//PAGAMENT D'UNA ENTRADA
		boolean pagamentEntrada(BigDecimal preu){
			boolean pagar;
			System.out.println("Import a pagar: " + preu + "€");
			System.out.println("Pagant... (2 seg)");
			pagar = Validacio.validaBoolea("\t Pagat? (S/N) ");
			if(pagar) {
				return true;
			}

			return false;

		}//pagamentEntrada


	@Override
	public void run() {
		
		compraEntradaPelicula();
		
		
	}

}
