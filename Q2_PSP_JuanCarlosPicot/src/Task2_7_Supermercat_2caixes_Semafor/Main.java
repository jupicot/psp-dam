package Task2_7_Supermercat_2caixes_Semafor;

import Task2_5_Supermercat_1caixa_waitNotify.Caixa;
import Task2_5_Supermercat_1caixa_waitNotify.Client;

public class Main {
	
	public static void main(String[] args)  throws InterruptedException {
		
		Caixa caixa = new Caixa("Caixa1");
		Caixa caixa2 = new Caixa("Caixa2");
		
		Client client1 = new Client("client1", caixa);
		Client client2 = new Client("client2", caixa2);
		Client client3 = new Client("client3", caixa);
		Client client4 = new Client("client4", caixa2);
		
		Thread fil1 = new Thread(client1, "client1");
		Thread fil2 = new Thread(client2, "client2");
		Thread fil3 = new Thread(client3, "client3");
		Thread fil4 = new Thread(client4, "client4");
		
		System.out.println("[" + Thread.currentThread().getName() + "]" + " Programa del supermercat amb 1 caixa");
		System.out.println("===========================================");
		System.out.println("Oberta la caixa " + "<" + caixa.getCaixa() + ">");
		System.out.println("Oberta la caixa " + "<" + caixa2.getCaixa() + ">");

		fil1.start();
		fil2.start();
		fil3.start();
		fil4.start();
		
		fil1.join();
		fil2.join();
		fil3.join();
		fil4.join();
		System.out.println("[" + Thread.currentThread().getName() + "]" + " Finalització fil ppal");
	}

}