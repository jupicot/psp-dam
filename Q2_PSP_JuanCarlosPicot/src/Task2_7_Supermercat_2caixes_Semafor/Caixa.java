package Task2_7_Supermercat_2caixes_Semafor;

public class Caixa{
	
	private String caixa;
	private boolean disponible;

	public Caixa() {
		super();
	}
	

	public Caixa(String caixa) {
		super();
		this.caixa = caixa;
		disponible = true;
	}


	public String getCaixa() {
		return caixa;
	}


	public void setCaixa(String caixa) {
		this.caixa = caixa;
	}
	
	
	
	public boolean isDisponible() {
		return disponible;
	}


	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}


	public void agafaCompra(String nomfil) {
		
		for(int i = 0; i < 10000; i++) {
			//Haciendo tiempo
		}
		System.out.println("<" + caixa + ">" + " Llegint la compra de " + nomfil);
		this.disponible = false;
	}
	
	public void cobramentCompra(String compra, String nomfil) {
		System.out.println("<" + caixa + ">" + " Import de la compra de " + nomfil + " es de " + compra  + "€");
		System.out.println("[" + nomfil + "]" + " Pagant la compra de " + compra + "€ a " + "<" + caixa + ">");
	}
	public void encuantseCola(String nomfil) {
		System.out.println("[" + nomfil + "]" + " Encuant-se amb la compra a " + "<" + caixa + ">");
	}
	
	public void ticketCompra(String nomfil) {
		System.out.println("<" + caixa + ">" + " Donant ticket compra a " + nomfil);
	}
	
	public void deixantCaixa(String nomfil) {
		System.out.println("[" + nomfil + "]" + " La caixa ja está disponible, desperta fils...");
		this.disponible = true;
	}
	
	public void recollintCompra(String nomfil) {
		System.out.println("[" + nomfil + "]" + " Recollint la compra a " + "<" + caixa + ">");
		System.out.println("[" + nomfil + "]" + " Eixint del supermercat");
	}
	
	public boolean isCaixaDisponible() {
		if(disponible) return true;
		else		   return false;
	}
	
	public synchronized void metodeWait(String nomfil) {
		try {
			while (!isCaixaDisponible()) {
				System.out.println("[" + nomfil + "]" + " La caixa no está disponible Zzz...");
				wait();
			}
			agafaCompra(nomfil);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void canviaestatdisponible(String nomfil) {
		deixantCaixa(nomfil);
		notifyAll();
	}

}