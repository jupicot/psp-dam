package CINE_v22_CompraNEntradesThreads;

import java.math.BigDecimal;
import java.util.ArrayList;

import CINE_v22_CompraNEntradesThreads.Sala;
import CINE_v22_CompraNEntradesThreads.Seient;
import CINE_v22_CompraNEntradesThreads.Sessio;
import CINE_v22_CompraNEntradesThreads.Validacio;
import CINE_v22_CompraNEntradesThreads.Pelicula;

public class filCompraNEntrades implements Runnable {
	
	int pelicula;
	ArrayList<Pelicula> pelicules;
	
	


	public filCompraNEntrades(int pelicula, ArrayList<Pelicula> pelicules) {
		super();
		this.pelicula = pelicula;
		this.pelicules = pelicules;
	}

	
	



	public int getPelicula() {
		return pelicula;
	}






	public void setPelicula(int pelicula) {
		this.pelicula = pelicula;
	}






	public ArrayList<Pelicula> getPelicules() {
		return pelicules;
	}






	public void setPelicules(ArrayList<Pelicula> pelicules) {
		this.pelicules = pelicules;
	}


	
	// ---------------------------------
			//COMPRA INTERACTIVA D'UNA UNICA ENTRADA
			public synchronized void compraNEntrades(){
				Pelicula p = null;
				Sala sa = null;
				Sessio se = null;
				//		String nsala, nsessio, npelicula, nfila, nseient;
				//		int sala, 
				int sessio, nEntrades; 

				
				//Selecció de PEL·LICULA
				p = pelicules.get(pelicula-1);
				System.out.println("\n\t"+p);
				System.out.println();
				System.out.println();

				//Si NO hi ha SESSIONS per la PEL·LICULA, s'ix del procés de compra
				System.out.println("\n\tLlista actual de SESSIONS de la PEL·LICULA:\n");
				if (p.llistarSessionsPeli()== 0) {		//Si NO hi ha SESSIONS per a la PEL·LICULA, s'ix
					System.out.println("\t ERROR Cine:compraNEntradesPelicula: No hi ha SESSIONS per a esta PEL·LICULA");
					System.out.println("\tAnul·lada acció");
					return;
				}
				
				//Selecció de la SESSIO
				sessio = Validacio.validaSencer("\n\t Tria la sessió per a "+p.getNomPeli()+":",p.getSessionsPeli().size());

				se = p.retornaSessioPeli(sessio);			//obté la SESSIO	
				sa = se.getSala();							//obté la SALA associada a la SESSIO
				se.mapaSessio();							//mostra el MAPA de la SESSIO
				Seient[][] seients = se.getSeients();		//obté la matriu de SEIENTS

				//Demana fila i Seient en fila per reservar-lo
				
				nEntrades = Validacio.validaSencer("\tTria cuantes entrades vols: ", 30);
				int[] fila = new int[sa.getFiles()];
				int[] seient = new int[sa.getTamanyFila()];
				for(int i=0;i<nEntrades;i++)
				{
					fila[i] = Validacio.validaSencer("\tTria FILA: [1-"+sa.getFiles()+"] ",sa.getFiles());
					seient[i] = Validacio.validaSencer("\t Tria SEIENT en la fila: [1-"+sa.getTamanyFila()+"]",sa.getTamanyFila());
					
					if (seients[fila[i]-1][seient[i]-1].verificaSeient()){ //Si SEIENT lliure -> reserva SEIENT
						//reserva el seient
						seients[fila[i]-1][seient[i]-1].reservaSeient();
						System.out.println("\n\tSeient reservat");
					}else{ //NO Reserva
						System.out.println("\t ERROR Cine:compraNEntradesPelicula: No sha pogut fer reserva Seient");
					};	

				}
					//pagament entrada
					if (pagamentNEntrades(se.getPreu(), nEntrades)) {
						for(int e = 0; e < nEntrades; e++) {
						seients[fila[e]-1][seient[e]-1].ocupaSeient();
						se.imprimirTicket(seients[fila[e]-1][seient[e]-1],se, sa, p);
						}
						System.out.println("\n\tSeients ocupats");
					}
					else {
						System.out.println("\t ERROR Cine:pagamentNEntrades: S'ha cancel·lat el pagament de l'entrada");
						for(int e = 0; e < nEntrades; e++) {
						seients[fila[e]-1][seient[e]-1].alliberaSeient();
						}
				}
				
				se.mapaSessio();
					
			}//compraNEntrades

			


			//*********************************************************
			//PAGAMENT D'ENTRADES
			boolean pagamentNEntrades(BigDecimal preu, int nEntrades){
				System.out.println("\tImport a pagar: "+preu.multiply(new BigDecimal(nEntrades))+" €");
				System.out.println("\n\tPagant...(2seg)");
				//pagant
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return Validacio.validaBoolea("\tPagat? (S/N)");
			}//pagamentNEntrades




	@Override
	public void run() {
		
		compraNEntrades();
		
	}

}
