package Task2_8_Supermercat_distribuidorCaixa;

public class Distribuidor {

	int numCaixes;
	int caixaActual;
	boolean caixaDisponible = true;
	Caixa caixa = new Caixa();
	public Distribuidor(int numCaixes) {
		super();
		this.numCaixes = numCaixes;
	}

	public synchronized void ocuparCaixa(String nombre) throws InterruptedException{
		while(numCaixes==0) {
			wait();
			System.out.println("<DISTRIBUIDOR> Cap caixa està disponible, dorm [" + nombre + "] zZzZz...");
		}
		caixaActual = numCaixes;

		
		System.out.println("<DISTRIBUIDOR> Assigna [" + nombre + "] a <CAIXA" + numCaixes + ">" );
		System.out.println("<CAIXA" + numCaixes + "> Atenent a " + nombre);
		numCaixes--;
	}
	
	public synchronized void finalitzarCompra(String nombre) {
		System.out.println("<CAIXA" + caixaActual + "> Finalitzada atenció " + nombre);
	}
	
	public synchronized void ixirCentre(String nombre) {
		System.out.println("[" + nombre + "] Client ix del centre comercial");
	}
	
	public synchronized void liberarCaixa(String nombre) {
		numCaixes++;
		System.out.println("<DISTRIBUIDOR> Desperta possibles fils dormits...");
		notify();
	}
}
