package Task2_8_Supermercat_distribuidorCaixa;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("[" + Thread.currentThread().getName() + "] Programa del distribuidor de caixes");
		System.out.println("=================================================================");
		
		
		Distribuidor monitor = new Distribuidor(3);
		Client client1 = new Client("client1", monitor);
		Client client2 = new Client("client2", monitor);
		Client client3 = new Client("client3", monitor);
		Client client4 = new Client("client4", monitor);
		Client client5 = new Client("client5", monitor);
		
		Thread fil1 = new Thread(client1, "client1");
		Thread fil2 = new Thread(client2, "client2");
		Thread fil3 = new Thread(client3, "client3");
		Thread fil4 = new Thread(client4, "client4");
		Thread fil5 = new Thread(client5, "client5");
		
		fil1.start();
		fil2.start();
		fil3.start();
		fil4.start();
		fil5.start();

		fil1.join();
		fil2.join();
		fil3.join();
		fil4.join();
		fil5.join();
		
		System.out.println("[" + Thread.currentThread().getName() + "] Finalització fil ppal");

		
	}

}
