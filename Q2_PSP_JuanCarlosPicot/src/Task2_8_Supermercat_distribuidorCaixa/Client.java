package Task2_8_Supermercat_distribuidorCaixa;

public class Client implements Runnable{

	float compra;
	String nombre;
	Distribuidor monitor;
	Caixa caixa = new Caixa();
	public Client(String nombre, Distribuidor monitor) {
		this.nombre = nombre;
		this.monitor = monitor;
	}
	
	public Client() {
	}
	
	public synchronized void anarCua(String nomfil) {
		System.out.println("[" + nomfil + "] Client va a cua distribuidor");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			compra = (float) (30 + Math.random() * (45 - 30));
			anarCua(Thread.currentThread().getName());
			monitor.ocuparCaixa(nombre);
			caixa.compra(nombre, compra);
			monitor.finalitzarCompra(nombre);
			monitor.liberarCaixa(nombre);
			monitor.ixirCentre(nombre);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
